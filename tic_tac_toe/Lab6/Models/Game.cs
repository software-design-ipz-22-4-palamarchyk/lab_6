﻿using System;

namespace Lab6.Models
{
    public class Game
    {
        private Board board;
        private char currentPlayer;

        public Game()
        {
            board = new Board();
            currentPlayer = 'X';
        }

        public void Play()
        {
            bool gameOn = true;

            while (gameOn)
            {
                board.DisplayBoard();
                Console.WriteLine($"Player {currentPlayer}'s turn.");
                Console.Write("Enter row (0, 1, 2): ");
                int row = int.Parse(Console.ReadLine());
                Console.Write("Enter column (0, 1, 2): ");
                int col = int.Parse(Console.ReadLine());

                if (board.MakeMove(row, col, currentPlayer))
                {
                    if (board.CheckWin(currentPlayer))
                    {
                        board.DisplayBoard();
                        Console.WriteLine($"Player {currentPlayer} wins!");
                        gameOn = false;
                    }
                    else if (board.IsFull())
                    {
                        board.DisplayBoard();
                        Console.WriteLine("The game is a draw!");
                        gameOn = false;
                    }
                    else
                    {
                        currentPlayer = currentPlayer == 'X' ? 'O' : 'X';
                    }
                }
                else
                {
                    Console.WriteLine("Invalid move. Try again.");
                }
            }
        }
    }
}
