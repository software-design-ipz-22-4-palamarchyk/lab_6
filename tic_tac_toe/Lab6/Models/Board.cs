﻿using System;

namespace Lab6.Models
{
    public class Board
    {
        private Cell[,] board;

        public Board()
        {
            board = new Cell[3, 3];
            InitializeBoard();
        }

        private void InitializeBoard()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    board[i, j] = new Cell();
                }
            }
        }

        public void DisplayBoard()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(board[i, j].Value);
                    if (j < 2) Console.Write("|");
                }
                Console.WriteLine();
                if (i < 2) Console.WriteLine("-----");
            }
        }

        public bool MakeMove(int row, int col, char player)
        {
            if (board[row, col].IsEmpty())
            {
                board[row, col].Value = player;
                return true;
            }
            return false;
        }

        public bool CheckWin(char player)
        {
            
            for (int i = 0; i < 3; i++)
            {
                if (board[i, 0].Value == player && board[i, 1].Value == player && board[i, 2].Value == player) return true;
                if (board[0, i].Value == player && board[1, i].Value == player && board[2, i].Value == player) return true;
            }
            if (board[0, 0].Value == player && board[1, 1].Value == player && board[2, 2].Value == player) return true;
            if (board[0, 2].Value == player && board[1, 1].Value == player && board[2, 0].Value == player) return true;

            return false;
        }

        public bool IsFull()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (board[i, j].IsEmpty()) return false;
                }
            }
            return true;
        }
    }
}
