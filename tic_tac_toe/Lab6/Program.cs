﻿using System;
using Lab6.Models; 

namespace Lab6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var game = new Game();
            game.Play();
        }
    }
}
